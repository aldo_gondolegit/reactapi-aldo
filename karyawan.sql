-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 09 Jul 2020 pada 04.11
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `karyawan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `daftarkaryawan`
--

CREATE TABLE `daftarkaryawan` (
  `id` int(11) NOT NULL,
  `nik` int(11) NOT NULL,
  `agama` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `jk` varchar(255) DEFAULT NULL,
  `kewarganegaraan` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `ttl` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `daftarkaryawan`
--

INSERT INTO `daftarkaryawan` (`id`, `nik`, `agama`, `alamat`, `jk`, `kewarganegaraan`, `name`, `ttl`) VALUES
(1, 2020, 'ISLAM', 'JALAN', 'Laki Laki', 'WNI', 'ALDO', 'Jakarta 20 Feb 2020'),
(36, 12154, 'Islam', 'Jalan Pagedangan', 'Perempuan', 'WNI', 'Risa', 'Karawang, 10 Januari 1995'),
(33, 292929, 'ISLAM TULEN', 'JALAN CIBODAS', 'LAKI LAKI', 'WNI', 'ANGGI IRAWAN', 'LAMPUNG, 13 Maret 2002'),
(34, 19219192, 'KRISTEN', 'MODERNLAND', 'LAKI-LAKI', 'WNI', 'MUEL', 'JAKARTA'),
(35, 20177284, 'ISLAM', 'JALAN CIBODAS', 'laki-laki', 'WNI', 'KAK RENDY SUPONO', 'Semarang, 30 April 1990'),
(31, 20177284, 'islam', 'jalan salak', 'laki-laki', 'WNA', 'BAGUS KEREN', 'WONOGIRI'),
(38, 11111, 'Islam', 'Jalan Bhayangkara', 'Laki-Laki', 'WNI', 'Fadlan Rizki', 'Bekasi, 19 Mei 2002 '),
(39, 101010, 'ISLAM', 'Jalan Jalan', 'laki-laki', 'WNI', 'Kak Wawan', 'Bandung');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(7);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `daftarkaryawan`
--
ALTER TABLE `daftarkaryawan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `daftarkaryawan`
--
ALTER TABLE `daftarkaryawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
